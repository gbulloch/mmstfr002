﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sage.Accounting.POP;

namespace Eureka.MMSTFR002.GenerateOrderPlus
{
    // A delegate type for hooking up change notifications.
    public delegate void ItemConfirmedEventHandler(object sender, POPItemEventArgs e);

    /// <summary>
    /// POP Item EventArgs used when confirming or unconfirming and item.
    /// </summary>
    public class POPItemEventArgs : EventArgs
    {
        // Fields
        private readonly POPToReorderWarehouse _POPItem;

        // Methods
        public POPItemEventArgs(POPToReorderWarehouse POPItem)
        {
            this._POPItem = POPItem;
        }

        // Properties
        public POPToReorderWarehouse POPItem
        {
            get
            {
                return this._POPItem;
            }
        }
    }
}
