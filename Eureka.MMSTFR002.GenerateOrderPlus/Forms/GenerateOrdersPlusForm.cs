using System;
using System.Drawing;
using System.Windows.Forms;
using Sage.Accounting.POP;
using Sage.Accounting.PurchaseLedger;
using Sage.Accounting.Stock;
using Sage.Common.Data;
using Sage.MMS.Controls;
using Sage.ObjectStore;
using Sage.ObjectStore.Controls;
using System.Reflection;
using Sage.Accounting.Common;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace Eureka.MMSTFR002.GenerateOrderPlus
{
    /// <summary>
    /// Summary description for GenerateOrdersPlusForm.
    /// </summary>
    public class GenerateOrdersPlusForm : Sage.MMS.POP.GenerateOrdersForm
    {
        private Sage.Accounting.Common.ActiveLock activeLock = new ActiveLock();

        private Grid oGrid;
        private GridColumn grdCol_ProductGroup;
        private GridColumn grdCol_Supplier;
        private GridColumn grdCol_SupplierPartRef;
        private NumberTextBox totalNumberTextBox;

        private List<POPToReorderItem> oConfirmedItems = new List<POPToReorderItem>();
        private List<POPToReorderItem> oConfirmedOrderItems = new List<POPToReorderItem>();

        private ProductGroup oProductGroup;
        private ProductGroupsLookup oProductGroupLookup;

        private Supplier oSupplier;
        private SupplierLookup oSupplierLookup;

        private Sage.Common.Controls.Button btn_ConfirmAll;
        private Sage.Common.Controls.Button btn_UnconfirmAll;
        private Sage.Common.Controls.Button btn_ResetFilters;
        private Sage.Common.Controls.Button btn_UpdateOrderList;

        private MMSTFR002Settings oSettings;
        private MMSTFR002Setting oSetting;

        private POPToReorderItems oPOPToReorderItems;

        private bool addingNewLine = false;
        private int analysisCode = 0;
        private decimal orderTotal = 0;
        private String oUser = Sage.Accounting.Application.ActiveUserName;
        private Sage.Accounting.Common.AnalysisCodeMappingCollection oMappings = null;

        // This event can be used by bespoke to access the items that are bing confirmed and make any changes that
        // may be required.  i.e. Update Unit Price
        public event ItemConfirmedEventHandler ItemConfirmed;
        // To Access add a reference to the Eureka.MMS116.GenerateOrderPlus.dll and hook onto the envent like this:
        //
        // ((Eureka.MMS116.GenerateOrderPlus.GenerateOrdersPlusForm)oForm.UnderlyingControl).ItemConfirmed += AddonForGenOrdersPlusForm_ItemConfirmed;
        //
        // then add a method to handle the event.  The item being confirmed is provided in the EventArgs
        //
        // private void AddonForGenOrdersPlusForm_ItemConfirmed(object sender, MMS116.GenerateOrderPlus.POPItemEventArgs e)
        // { // Do Stuff Here with e.POPItem }
        //

        private EurekaPOPToReorderItems oEurekaPOPToReorderItems;
#if !S200v9 && !S200v92 && !S200v10 && !S200v11 && !S200v12
		private int sortColumn;
#endif

        public GenerateOrdersPlusForm()
        {
            //Check the Manager Analysis Code has been set in the settings
            oSettings = new MMSTFR002Settings();

            if (oSettings.IsEmpty)
            {
                MessageBox.Show("Please setup,the Manager Analysis Code in the MMSTFR002 Settings", "MMSTFR002", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                oSetting = oSettings.First;
#if Licence_CP
                if (ControlPanel.AddonHelper.IsEnabled("MMSTFR002", ModuleType.Bespoke))
#else
			if (LicenceCheck.LicenceIsValid())
#endif
                {
#if !S200v9 && !S200v92 && !S200v10 && !S200v11 && !S200v12
				sortColumn = 0;
#endif
                    Initialise();
                }
                else
                    Close();
            }
        }

        private void Initialise()
        {
            this.Text = "Generate Purchase Orders - Bespoke";


            //Get the Supplier Analysis code saved in the MMSTFR002Settings
            oMappings = Sage.Accounting.Common.Factories.AnalysisCodeMappingCollectionFactory.Factory.FetchForEntityInput("Sage.Accounting.PurchaseLedger.Supplier");

            foreach (AnalysisCodeMapping oMapping in oMappings)
            {
                if (oMapping.AnalysisCode.AnalysisCodeDbKey == oSetting.AnalysisCode)
                {
                    //Retrieve the analysis code number
                    string Code = Regex.Replace(oMapping.PropertyName.ToString(), "[^0-9]+", string.Empty);
                    analysisCode = int.Parse(Code);
                    break;
                }
            }

            this.Width += 270;

            oEurekaPOPToReorderItems = new EurekaPOPToReorderItems();

            //Get the BeforeClick on the Update Order List button
            btn_UpdateOrderList = (Sage.Common.Controls.Button)this.GetControl("updateButton", this.buttonPanel);
            btn_UpdateOrderList.BeforeClick += Btn_UpdateOrderList_BeforeClick;

            Sage.Common.Controls.GroupBox stockItemsGroupBox = (Sage.Common.Controls.GroupBox)this.GetControl("stockItemsGroupBox", this.mainPanel);
            // Get the existing close button and change the tab index.

            Sage.Common.Controls.Button closeButton = (Sage.Common.Controls.Button)this.GetControl("closeButton", this.buttonPanel);
            closeButton.TabIndex = 5;

            // Get the existing generate orders button.
            Sage.Common.Controls.Button generateOrdersButton = (Sage.Common.Controls.Button)this.GetControl("generateOrdersButton", this.buttonPanel);
            generateOrdersButton.BeforeClick += GenerateOrdersButton_BeforeClick;
            generateOrdersButton.Click += new EventHandler(generateOrdersButton_Click);

            // Get the existing generate orders button.
            Sage.Common.Controls.Button addItemButton = (Sage.Common.Controls.Button)this.GetControl("addItemButton", this.itemButtonPanel);
            addItemButton.BeforeClick += addItemButton_BeforeClick;
            addItemButton.AfterClick += addItemButton_AfterClick;

            // Get the existing grid.
            oGrid = (Sage.ObjectStore.Controls.Grid)this.GetControl("stockItemGrid", this.mainPanel);
            oGrid.AllowSerialisation = false;
            oGrid.AlwaysShowVerticalScrollBar = true;

            // Add new panel to the stockItemsGroupbox and then add the labels and SupplierLookup and productGroupLookup
            // controls to the new panel. These controls will be used to filter the grid control.
            Sage.Common.Controls.Panel filterPanel = new Sage.Common.Controls.Panel();
            filterPanel.Height = 30;
            filterPanel.Name = "filterPanel";
            filterPanel.Dock = DockStyle.Top;
            stockItemsGroupBox.Controls.Add(filterPanel);

            // Add the Product Group label.
            Sage.Common.Controls.Label lbl_ProductGroup = new Sage.Common.Controls.Label();
            lbl_ProductGroup.Location = new Point(122, 11);
            lbl_ProductGroup.Name = "lbl_ProductGroup";
            lbl_ProductGroup.Text = "Product Group:";
            filterPanel.Controls.Add(lbl_ProductGroup);

            // Add the productGroupLookup control.
            oProductGroupLookup = new ProductGroupsLookup();
            oProductGroupLookup.Location = new Point(250, 6);
            oProductGroupLookup.Name = "oProductGroupLookup";
            oProductGroupLookup.Width = 150;
            filterPanel.Controls.Add(oProductGroupLookup);
            oProductGroupLookup.ProductGroupChanged += new EventHandler(oProductGroupLookup_ProductGroupChanged);

            // Add the SupplierLookup control.
            oSupplierLookup = new SupplierLookup();
            oSupplierLookup.Location = new Point(450, 6);
            oSupplierLookup.Name = "oSupplierLookup";
            filterPanel.Controls.Add(oSupplierLookup);
            oSupplierLookup.SupplierChanged += new EventHandler(oSupplierLookup_SupplierChanged);

            // Add the Reset Filter button
            btn_ResetFilters = new Sage.Common.Controls.Button();
            btn_ResetFilters.Name = "btn_ResetFilters";
            btn_ResetFilters.Text = "Clear Filter";
            btn_ResetFilters.Location = new Point(750, 6);
            filterPanel.Controls.Add(btn_ResetFilters);
            btn_ResetFilters.Click += new EventHandler(btn_ResetFilters_Click);

            // Add the Confirm All button.
            btn_ConfirmAll = new Sage.Common.Controls.Button();
            btn_ConfirmAll.Text = "Confirm All";
            btn_ConfirmAll.Name = "btn_ConfirmAll";
            btn_ConfirmAll.TabIndex = 3;
            this.buttonPanel.Controls.Add(btn_ConfirmAll);
            btn_ConfirmAll.Click += new EventHandler(btn_ConfirmAll_Click);
            btn_ConfirmAll.AfterClick += Btn_ConfirmAll_AfterClick;

            // Add the UnConfirm All button.
            btn_UnconfirmAll = new Sage.Common.Controls.Button();
            btn_UnconfirmAll.Text = "Unconfirm All";
            btn_UnconfirmAll.Name = "btn_UnconfirmAll";
            btn_UnconfirmAll.TabIndex = 4;
            buttonPanel.Controls.Add(btn_UnconfirmAll);
            btn_UnconfirmAll.Click += new EventHandler(btn_UnconfirmAll_Click);


            Panel rightDetailPanel = (Sage.Common.Controls.Panel)this.GetControl("rightDetailPanel");
            NumberTextBox totalConfirmedNumberTextBox = (Sage.ObjectStore.Controls.NumberTextBox)GetControl("totalConfirmedNumberTextBox");
            totalNumberTextBox = new NumberTextBox();
            totalNumberTextBox.Name = "totalNumberTextBox";
            totalNumberTextBox.TabIndex = 5;
            rightDetailPanel.Controls.Add(totalNumberTextBox);
            totalNumberTextBox.Location = totalConfirmedNumberTextBox.Location;
            totalNumberTextBox.BringToFront();
            totalNumberTextBox.Enabled = false;
            

            // Create and configure the Product Group column for the new grid control.
            grdCol_ProductGroup = new GridColumn();
            grdCol_ProductGroup.Caption = "Product Group";
            grdCol_ProductGroup.Width = 90;
#if S200v9 || S200v92 || S200v10 || S200v11 || S200v12
            grdCol_ProductGroup.DisplayMember = "Item.ProductGroup.Code";
#else
			grdCol_ProductGroup.DisplayMember = "ProductGroupCode";
#endif
            grdCol_ProductGroup.ContentAlignment = ContentAlignment.MiddleLeft;
            oGrid.Columns.Insert(2, grdCol_ProductGroup);

            // Create and configure the Supplier column for the new grid control.
            grdCol_Supplier = new GridColumn();
            grdCol_Supplier.Caption = "Supplier";
            grdCol_Supplier.Width = 90;
#if S200v9 || S200v92 || S200v10 || S200v11 || S200v12
            grdCol_Supplier.DisplayMember = "Item.PreferredSupplier.Supplier.Reference";
#else
			grdCol_Supplier.DisplayMember = "SupplierReference";
#endif
            grdCol_Supplier.ContentAlignment = ContentAlignment.MiddleLeft;
            oGrid.Columns.Insert(3, grdCol_Supplier);

            // Create and configure the Supplier Part Ref column for the new grid control.
            grdCol_SupplierPartRef = new GridColumn();
            grdCol_SupplierPartRef.Caption = "Supplier Ref";
            grdCol_SupplierPartRef.Width = 90;
#if S200v9 || S200v92 || S200v10 || S200v11 || S200v12
            grdCol_SupplierPartRef.DisplayMember = "Item.PreferredSupplier.SupplierStockCode";
#else
			grdCol_SupplierPartRef.DisplayMember = "SupplierPartRef";
#endif
            grdCol_SupplierPartRef.ContentAlignment = ContentAlignment.MiddleLeft;
            oGrid.Columns.Insert(4, grdCol_SupplierPartRef);

            // Add the event handlers.
            oGrid.DataSource = this.oEurekaPOPToReorderItems;
            oGrid.DataSourceChanged += new EventHandler(oGrid_DataSourceChanged);

            // Get the FormClosing Event
            this.FormClosing += GenerateOrdersPlusForm_FormClosing;
            this.Load += GenerateOrdersPlusForm_Load;

#if !S200v9 && !S200v92 && !S200v10 && !S200v11 && !S200v12
			oGrid.ColumnClick += new Sage.Common.Controls.ColumnClickEventHandler(oGrid_ColumnClick);
			this.Closed += new EventHandler(this_Closed);
#endif
            ApplyFilter();
        }

        private void Btn_ConfirmAll_AfterClick(object sender, EventArgs e)
        {
            CalculateTotal();
        }

        private void GenerateOrdersPlusForm_Load(object sender, EventArgs e)
        {
            CalculateTotal();
        }

        private void CalculateTotal()
        {
            EurekaPOPToReorderItems oList = (EurekaPOPToReorderItems)this.oGrid.DataSource;

            this.Cursor = Cursors.WaitCursor;

            orderTotal = 0;

            try
            {
                foreach (EurekaPOPToReorderItem oEurekaPOPToReorderItem in oList)
                {
                    try
                    {
                        // Cycle through all warehouses for the item and set the confirmed flag.
                        foreach (POPToReorderWarehouse oPOPToReorderWarehouse in oEurekaPOPToReorderItem.ToReorderWarehouses)
                        {
                            if (oEurekaPOPToReorderItem.ConfirmedPurchaseValue > 0 && oEurekaPOPToReorderItem.Fields["AnalysisCode" + analysisCode].Value.ToString() == this.oUser)
                            {
                                orderTotal += oPOPToReorderWarehouse.ConfirmedPurchaseValue;
                            }
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
                orderTotal = decimal.Round(orderTotal, 2);
                totalNumberTextBox.Text = orderTotal.ToString();

                ApplyFilter();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void GenerateOrdersButton_BeforeClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            oEurekaPOPToReorderItems = new EurekaPOPToReorderItems();

            if (oEurekaPOPToReorderItems.First != null)
            {
                this.Cursor = Cursors.WaitCursor;

                try
                {


                    foreach (EurekaPOPToReorderItem oEurekaPOPToReorderItem in oEurekaPOPToReorderItems)
                    {
                        try
                        {
                            // Cycle through all warehouses for the item and set the confirmed flag.
                            foreach (POPToReorderWarehouse oPOPToReorderWarehouse in oEurekaPOPToReorderItem.ToReorderWarehouses)
                            {
                                if (oPOPToReorderWarehouse.Confirmed && oEurekaPOPToReorderItem.Fields["AnalysisCode" + analysisCode].Value.ToString() != this.oUser)
                                {
                                    activeLock = Sage.Accounting.Application.Lock(oPOPToReorderWarehouse);
                                    oConfirmedOrderItems.Add(oEurekaPOPToReorderItem);
                                    oPOPToReorderWarehouse.Confirmed = false;

                                    POPItemEventArgs _Args = new POPItemEventArgs(oPOPToReorderWarehouse);
                                    OnItemConfirmed(_Args);

                                    oPOPToReorderWarehouse.Update();
                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
                finally
                {
                    this.oEurekaPOPToReorderItems.Refresh();
                    this.Cursor = Cursors.Default;

                    RefreshTotal();
                }
            }
        }

        private void GenerateOrdersPlusForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (oConfirmedItems != null || oConfirmedOrderItems != null)
            {

                try
                {
                    if (oConfirmedItems.Count > 0)
                    {
                        // Loops through Items that were previously confirmed and re-confirms them
                        foreach (POPToReorderItem oPOPToReorderItem in oConfirmedItems)
                        {
                            foreach (POPToReorderWarehouse oPOPToReorderWarehouse in oPOPToReorderItem.ToReorderWarehouses)
                            {
                                activeLock = Sage.Accounting.Application.Lock(oPOPToReorderWarehouse);
                                oPOPToReorderWarehouse.Confirmed = true;
                                oPOPToReorderWarehouse.Update();

                            }
                        }
                    }

                    if (oConfirmedOrderItems.Count > 0)
                    {
                        // Loops through Items that were previously confirmed and re-confirms them
                        foreach (POPToReorderItem oPOPToReorderItem in oConfirmedOrderItems)
                        {
                            foreach (POPToReorderWarehouse oPOPToReorderWarehouse in oPOPToReorderItem.ToReorderWarehouses)
                            {
                                activeLock = Sage.Accounting.Application.Lock(oPOPToReorderWarehouse);
                                oPOPToReorderWarehouse.Confirmed = true;
                                oPOPToReorderWarehouse.Update();

                            }
                        }
                    }
                }
                finally
                {
                    if (activeLock != null)
                    {
                        activeLock.Dispose();
                        activeLock = null;
                    }
                }

            }
        }

        private void Btn_UpdateOrderList_BeforeClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            oPOPToReorderItems = new POPToReorderItems();


            this.Cursor = Cursors.WaitCursor;

            try
            {
                //Loop through all items to be re-ordered
                foreach (POPToReorderItem oPOPToReorderItem in oPOPToReorderItems)
                {
                    try
                    {
                        //Loop through the Warehouses for each item
                        foreach (POPToReorderWarehouse oPOPToReorderWarehouse in oPOPToReorderItem.ToReorderWarehouses)
                        {
                            // If confirmed, lock the order, add the item to a collection and set the Confirmed Status to false
                            if (oPOPToReorderWarehouse.Confirmed)
                            {
                                activeLock = Sage.Accounting.Application.Lock(oPOPToReorderWarehouse);
                                oConfirmedItems.Add(oPOPToReorderItem);
                                oPOPToReorderWarehouse.Confirmed = false;
                                oPOPToReorderWarehouse.Update();
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                        continue;
                    }
                    finally
                    {

                        if (activeLock != null)
                        {
                            activeLock.Dispose();
                            activeLock = null;
                        }

                    }
                }
            }
            finally
            {
                ApplyFilter();
                //this.oEurekaPOPToReorderItems.Refresh();
                this.Cursor = Cursors.Default;

                RefreshTotal();
            }
        }


        private void addItemButton_AfterClick(object sender, EventArgs e)
        {
            addingNewLine = false;
        }

        private void addItemButton_BeforeClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            addingNewLine = true;
        }

        private void btn_ResetFilters_Click(object sender, EventArgs e)
        {
            oProductGroupLookup.ProductGroup = null;
            oSupplierLookup.Supplier = null;
            ApplyFilter();
        }

        //private void deleteItemButton_Click(object sender, EventArgs e)
        //{
        //    foreach (Sage.Common.Controls.ListItem oListItem in oGrid.SelectedItems)
        //    {
        //        if (oListItem.DataSource is EurekaPOPToReorderItem)
        //        {
        //            EurekaPOPToReorderItem oEurekaPOPToReorderItem = (EurekaPOPToReorderItem)oListItem.DataSource;
        //            try
        //            {
        //                oEurekaPOPToReorderItem.Delete();
        //            }
        //            catch (Exception ex)
        //            {
        //                MessageBox.Show(ex.ToString());
        //            }
        //        }
        //    }
        //}

        private void oProductGroupLookup_ProductGroupChanged(object sender, EventArgs e)
        {
            // Filter by product group.
            this.oProductGroup = oProductGroupLookup.ProductGroup;
            ApplyFilter();
        }

        private void oSupplierLookup_SupplierChanged(object sender, EventArgs e)
        {
            // Filter by supplier.
            this.oSupplier = oSupplierLookup.Supplier;
            ApplyFilter();
        }

        private void ApplyFilter()
        {
            // Add the filters to the collection.
            this.oEurekaPOPToReorderItems.Query.Filters.Clear();
            if (this.oProductGroup != null && !this.oProductGroup.ProductGroup.IsNull)
            {
                this.oEurekaPOPToReorderItems.Query.Filters.Add(new Filter(EurekaPOPToReorderItem.FIELD_PRODUCTGROUPCODE, FilterOperator.Equal, this.oProductGroup.Code));
            }
            if (this.oSupplier != null && !this.oSupplier.PLSupplierAccount.IsNull)
            {
                this.oEurekaPOPToReorderItems.Query.Filters.Add(new Filter(EurekaPOPToReorderItem.FIELD_SUPPLIERREFERENCE, FilterOperator.Equal, this.oSupplier.Reference));
            }

            //Filter the Analysis Code in the settings by the acrtive user
            this.oEurekaPOPToReorderItems.Query.Filters.Add(new Filter("AnalysisCode" + analysisCode, oUser));

            this.oEurekaPOPToReorderItems.Find();
        }

        private void generateOrdersButton_Click(object sender, EventArgs e)
        {
            // After orders are generated, reset the form.
            oSupplierLookup.Supplier = null;
            oProductGroupLookup.ProductGroup = null;
            ApplyFilter();
        }

        private void oGrid_DataSourceChanged(object sender, EventArgs e)
        {

#if S200v9 || S200v92 || S200v10 || S200v11 || S200v12
            if (this.oGrid.DataSource != null && this.oGrid.DataSource != this.oEurekaPOPToReorderItems)
#else
			if (this.oGrid.DataSource != this.oEurekaPOPToReorderItems)
#endif
            {
#if S200v9 || S200v92 || S200v10 || S200v11 || S200v12


                this.oEurekaPOPToReorderItems.Update();


                //We need to loop round and update any POPToReorderItems incase they've been confirmed on the other screen as 2013 onwards no longer automatically does it.
                //Not if adding a new line as this will revert the changes back
                if (!addingNewLine)
                {
                    foreach (EurekaPOPToReorderItem oItem in oEurekaPOPToReorderItems)
                    {
                        try
                        {
                            Sage.Accounting.POP.POPToReorderItem item = (Sage.Accounting.POP.POPToReorderItem)oItem;


                            Sage.Accounting.POP.POPToReorderItems oPOPToReorderItems = new Sage.Accounting.POP.POPToReorderItems();


                            oPOPToReorderItems.Query.Filters.Add(new Filter(Sage.Accounting.POP.POPToReorderItem.FIELD_POPTOREORDERITEM, item.POPToReorderItem));
                            oPOPToReorderItems.Find();
                            if (!oPOPToReorderItems.IsEmpty)
                            {
                                oPOPToReorderItems.First.QuantityConfirmed = item.QuantityConfirmed;
                                oPOPToReorderItems.First.QuantityRequired = item.QuantityRequired;
                                oPOPToReorderItems.First.ToReorderItemStatusDbKey = item.ToReorderItemStatusDbKey;
                                oPOPToReorderItems.First.ConfirmedPurchaseValue = item.ConfirmedPurchaseValue;
                                oPOPToReorderItems.Update();
                            }
                        }
                        catch (Exception x)
                        { /*Sage.Common.MessageBox.SageMessageBox.Show("Eror updating POPToReorderItems: " + x.Message.ToString());*/ }
                    }
                }

                ////this.oEurekaPOPToReorderItems.Find();
                //this.oEurekaPOPToReorderItems = new EurekaPOPToReorderItems();


                this.oEurekaPOPToReorderItems.Find();
#endif



                this.oGrid.DataSource = this.oEurekaPOPToReorderItems;

                CalculateTotal();

                System.Diagnostics.Debug.WriteLine("Reset Grid DataSource");
            }
        }

#if !S200v9 && !S200v92 && !S200v10 && !S200v11 && !S200v12
		private void oGrid_ColumnClick(object sender, Sage.Common.Controls.ListColumnClickEventArgs args)
		{
			bool sortascending = true;
			// The column headers are used to sort the columns.
			// If the selected column does not match the currently sorted column, then sort the selected column in ascending order, else
			// sort in descending order.
			if (args.Column == grdCol_ProductGroup.Index || args.Column == grdCol_Supplier.Index || args.Column == grdCol_SupplierPartRef.Index)
			{
				if (args.Column != sortColumn || !sortascending)
				{
					sortascending = true;
				}
				else
				{
					sortascending = false;
				}
			}
			Sort(args.Column, sortascending);
			// Set sortColumn to the selected column.
			sortColumn = args.Column;
		}

		private void Sort(int Column, bool Direction)
		{
			// Sorts the passed column number by ascending order if Direction is true else descending order if Direction is false.
			// Add the sort to the collection
			oEurekaPOPToReorderItems.Query.Sorts.Clear();
			switch (Column)
			{
				case 2:
					{
						oEurekaPOPToReorderItems.Query.Sorts.Add(new Sort(EurekaPOPToReorderItem.FIELD_PRODUCTGROUPCODE, Direction));
						break;
					}
				case 3:
					{
						oEurekaPOPToReorderItems.Query.Sorts.Add(new Sort(EurekaPOPToReorderItem.FIELD_SUPPLIERREFERENCE, Direction));
						break;
					}
				case 4:
					{
						oEurekaPOPToReorderItems.Query.Sorts.Add(new Sort(EurekaPOPToReorderItem.FIELD_SUPPLIERPARTREF, Direction));
						break;
					}
				default:
					{
						break;
					}

			}
			oEurekaPOPToReorderItems.Find();
			// Set the column header sort arrow.
			if (Direction)
			{
				oGrid.Columns[Column].Sort = Sage.Common.Controls.Sort.Descending;
			}
			else
			{
				oGrid.Columns[Column].Sort = Sage.Common.Controls.Sort.Ascending;
			}
		}
#endif

        private void btn_ConfirmAll_Click(object sender, EventArgs args)
        {
            // Cycle through all items in the collection and set the confirmed flag.
            if (oEurekaPOPToReorderItems.First != null)
            {
                this.Cursor = Cursors.WaitCursor;

                try
                {
                    foreach (EurekaPOPToReorderItem oEurekaPOPToReorderItem in this.oEurekaPOPToReorderItems)
                    {
                        try
                        {
                            // Cycle through all warehouses for the item and set the confirmed flag.
                            foreach (POPToReorderWarehouse oPOPToReorderWarehouse in oEurekaPOPToReorderItem.ToReorderWarehouses)
                            {
                                if (!oPOPToReorderWarehouse.Confirmed)
                                {
                                    oPOPToReorderWarehouse.Confirmed = true;

                                    POPItemEventArgs _Args = new POPItemEventArgs(oPOPToReorderWarehouse);
                                    OnItemConfirmed(_Args);

                                    oPOPToReorderWarehouse.Update();
                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
                finally
                {
                    this.oEurekaPOPToReorderItems.Refresh();
                    this.Cursor = Cursors.Default;

                    RefreshTotal();
                }
            }
        }

        private void btn_UnconfirmAll_Click(object sender, EventArgs args)
        {
            // Cycle through all items in the collection and set the confirmed flag.
            if (oEurekaPOPToReorderItems.First != null)
            {
                this.Cursor = Cursors.WaitCursor;





                try
                {
                    foreach (EurekaPOPToReorderItem oEurekaPOPToReorderItem in this.oEurekaPOPToReorderItems)
                    {
                        try
                        {


                            // Cycle through all warehouses for the item and set the confirmed flag.
                            foreach (POPToReorderWarehouse oPOPToReorderWarehouse in oEurekaPOPToReorderItem.ToReorderWarehouses)
                            {
                                if (oPOPToReorderWarehouse.Confirmed)
                                {
                                    oPOPToReorderWarehouse.Confirmed = false;
                                    oPOPToReorderWarehouse.Update();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex.Message);
                            continue;
                        }
                    }


                }
                finally
                {
                    
                    ApplyFilter();
                    //this.oEurekaPOPToReorderItems.Refresh();
                    this.Cursor = Cursors.Default;
                    RefreshTotal();

                    totalNumberTextBox.Text = "0";
                }
            }
        }

        private void RefreshTotal()
        {
            POPBackToBackCoordinator coord = (POPBackToBackCoordinator)((Sage.ObjectStore.Controls.NumberTextBox)GetControl("totalConfirmedNumberTextBox")).DataBindings[0].DataSource;

#if S200v9 || S200v92 || S200v10 || S200v11 || S200v12
            coord.ToReorderItems.Find();
            coord.RefreshTotalConfirmedValue();
#else
			Type type = coord.GetType();
			type.InvokeMember("RefreshTotalConfirmedValue",
				System.Reflection.BindingFlags.InvokeMethod | System.Reflection.BindingFlags.NonPublic |
				System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance,
				null, coord, null);

#endif
            this.RefreshDataBindings(coord);
        }

        private Control GetControl(string controlname)
        {
            return GetControl(controlname, this);
        }

        private Control GetControl(string controlname, Control ctrl)
        {
            if (ctrl.Name == controlname)
            {
                return ctrl;
            }
            foreach (Control ctrl1 in ctrl.Controls)
            {
                if (ctrl1.Name == controlname)
                {
                    return ctrl1;
                }
                if (ctrl1.Controls.Count > 0)
                {
                    Control ctrl2 = GetControl(controlname, ctrl1);
                    if (ctrl2 != null)
                    {
                        return ctrl2;
                    }
                }
            }
            return null;
        }

        public void OnItemConfirmed(POPItemEventArgs e)
        {
            if (ItemConfirmed != null)
                ItemConfirmed(this, e);
        }

#if !S200v9 && !S200v92 && !S200v10 && !S200v11 && !S200v12
		public void this_Closed(object sender, EventArgs args)
		{
			// Dispose of all objects.
			oGrid.Dispose();
			oGrid = null;
			grdCol_ProductGroup.Dispose();
			grdCol_ProductGroup = null;
			grdCol_Supplier.Dispose();
			grdCol_Supplier = null;
			grdCol_SupplierPartRef.Dispose();
			grdCol_SupplierPartRef = null;
			btn_ConfirmAll.Dispose();
			btn_ConfirmAll = null;
			btn_UnconfirmAll.Dispose();
			btn_UnconfirmAll = null;
			oProductGroupLookup.Dispose();
			oProductGroupLookup = null;
			oSupplierLookup.Dispose();
			oSupplierLookup = null;
		}
#endif
    }


}
