﻿namespace Eureka.MMSTFR002.GenerateOrderPlus
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new Sage.Common.Controls.GroupBox();
            this.analysisCodeMappingLookup1 = new Sage.MMS.Controls.AnalysisCodeMappingLookup();
            this.label1 = new Sage.Common.Controls.Label();
            this.buttonPanel1 = new Sage.Common.Controls.ButtonPanel();
            this.btnSave = new Sage.Common.Controls.Button();
            this.btn_Close = new Sage.Common.Controls.Button();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPanel)).BeginInit();
            this.buttonPanel.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.panelContent.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.analysisCodeMappingLookup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPanel1)).BeginInit();
            this.buttonPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonPanel
            // 
            this.buttonPanel.Controls.Add(this.buttonPanel1);
            this.buttonPanel.Location = new System.Drawing.Point(0, 173);
            this.buttonPanel.Padding = new System.Windows.Forms.Padding(10, 11, 10, 11);
            this.buttonPanel.Size = new System.Drawing.Size(465, 48);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.groupBox1);
            this.mainPanel.Location = new System.Drawing.Point(0, 11);
            this.mainPanel.Padding = new System.Windows.Forms.Padding(10, 0, 10, 11);
            this.mainPanel.Size = new System.Drawing.Size(465, 161);
            // 
            // panelContent
            // 
            this.panelContent.Location = new System.Drawing.Point(5, 35);
            this.panelContent.Padding = new System.Windows.Forms.Padding(0, 11, 0, 0);
            this.panelContent.Size = new System.Drawing.Size(465, 221);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.analysisCodeMappingLookup1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.IgnoreTextFormatting = false;
            this.groupBox1.Location = new System.Drawing.Point(10, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(445, 150);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Analysis Code";
            // 
            // analysisCodeMappingLookup1
            // 
            this.analysisCodeMappingLookup1.AllowSinglePartialMatch = true;
            this.analysisCodeMappingLookup1.BackColor = System.Drawing.SystemColors.Window;
            this.analysisCodeMappingLookup1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.analysisCodeMappingLookup1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.analysisCodeMappingLookup1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.analysisCodeMappingLookup1.Location = new System.Drawing.Point(118, 89);
            this.analysisCodeMappingLookup1.MaxLength = 32767;
            this.analysisCodeMappingLookup1.Name = "analysisCodeMappingLookup1";
            this.analysisCodeMappingLookup1.Size = new System.Drawing.Size(190, 21);
            this.analysisCodeMappingLookup1.TabIndex = 3;
            this.analysisCodeMappingLookup1.Tooltip = "";
            this.analysisCodeMappingLookup1.Value = null;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.IgnoreTextFormatting = false;
            this.label1.Location = new System.Drawing.Point(34, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(318, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please select the Analysis Code used to store the User Manager:";
            this.label1.ToolTip = "";
            // 
            // buttonPanel1
            // 
            this.buttonPanel1.Controls.Add(this.btnSave);
            this.buttonPanel1.Controls.Add(this.btn_Close);
            this.buttonPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonPanel1.Location = new System.Drawing.Point(10, 11);
            this.buttonPanel1.Name = "buttonPanel1";
            this.buttonPanel1.Orientation = Sage.Common.Controls.ButtonPanelOrientation.Horizontal;
            this.buttonPanel1.Size = new System.Drawing.Size(445, 26);
            this.buttonPanel1.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(0, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(65, 26);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.ToolTipText = "Save the information.";
            this.btnSave.Type = Sage.Common.Controls.ButtonType.Save;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Alignment = Sage.Common.Controls.ButtonAlignmentInContainer.Right;
            this.btn_Close.ControlAlignment = Sage.Common.Controls.ControlAlignmentInContainer.Right;
            this.btn_Close.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Close.Location = new System.Drawing.Point(380, 0);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(65, 26);
            this.btn_Close.TabIndex = 1;
            this.btn_Close.Text = "&Close";
            this.btn_Close.ToolTipText = "Close this form.";
            this.btn_Close.Type = Sage.Common.Controls.ButtonType.Close;
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 261);
            this.MinimumSize = new System.Drawing.Size(491, 300);
            this.Name = "SettingsForm";
            this.Text = "MMSTFR002 Settings Form";
            ((System.ComponentModel.ISupportInitialize)(this.buttonPanel)).EndInit();
            this.buttonPanel.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.panelContent.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.analysisCodeMappingLookup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPanel1)).EndInit();
            this.buttonPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sage.Common.Controls.ButtonPanel buttonPanel1;
        private Sage.Common.Controls.Button btnSave;
        private Sage.Common.Controls.Button btn_Close;
        private Sage.Common.Controls.GroupBox groupBox1;
        private Sage.MMS.Controls.AnalysisCodeMappingLookup analysisCodeMappingLookup1;
        private Sage.Common.Controls.Label label1;
    }
}