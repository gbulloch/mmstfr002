﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sage.MMS;
using Sage.ObjectStore;
using Sage.Accounting.PurchaseLedger;

namespace Eureka.MMSTFR002.GenerateOrderPlus
{
    public partial class SettingsForm : BaseForm
    {
        MMSTFR002Settings oSettings;
        MMSTFR002Setting oSetting;


        public SettingsForm()
        {
#if Licence_CP
            if (ControlPanel.AddonHelper.IsEnabled("MMSTFR002", ModuleType.Bespoke))
#else
            if (LicenceCheck.LicenceIsValid())
#endif
            {
                InitializeComponent();
                SetupForm();
            }
            else
                Close();
        }

        private void SetupForm()
        {
            analysisCodeMappingLookup1.ClassName = "Sage.Accounting.PurchaseLedger.Supplier";

            oSettings = new MMSTFR002Settings();

            if (oSettings.IsEmpty)
                oSetting = new MMSTFR002Setting();
            else
                oSetting = oSettings.First;

            //Get the Supplier Analysis Codes
            Supplier oOrder = SupplierFactory.Factory.CreateNew();
            oOrder.AnalysisCodeMappings.Query.Filters.Add(new Filter(Sage.Accounting.Common.AnalysisCodeMapping.FIELD_ANALYSISCODEDBKEY, oSetting.AnalysisCode));
            oOrder.AnalysisCodeMappings.Find();
            if (!oOrder.AnalysisCodeMappings.IsEmpty)
                analysisCodeMappingLookup1.AnalysisCodeMapping = oOrder.AnalysisCodeMappings.First;



        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            oSetting.AnalysisCode = analysisCodeMappingLookup1.AnalysisCodeMapping.AnalysisCodeDbKey;
            oSetting.Update();

            MessageBox.Show("Settings saved successfully.", "MMSTFR002", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Close();
        }
    }
}
