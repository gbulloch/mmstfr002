namespace Eureka.MMSTFR002.GenerateOrderPlus
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using Sage.Common.Data;
    using Sage.ObjectStore;
    
    
    [Sage.ObjectStore.Builder.PersistentObjectAttribute(TableName="MMSTFR002Setting", Name="MMSTFR002Setting")]
    [System.Runtime.Serialization.DataContractAttribute(IsReference=true, Namespace="http://schemas.sage.com/sage200/2011/")]
    public class PersistentMMSTFR002Setting : Sage.ObjectStore.PersistentObject
    {
        
        /// <summary>
        /// MMSTFR002SettingID Field Name
        /// </summary>
        public const string FIELD_MMSTFR002SETTING = "MMSTFR002Setting";
        
        protected Sage.ObjectStore.Field _MMSTFR002Setting;
        
        /// <summary>
        /// AnalysisCode Field Name
        /// </summary>
        public const string FIELD_ANALYSISCODE = "AnalysisCode";
        
        protected Sage.ObjectStore.Field _AnalysisCode;
        
        public PersistentMMSTFR002Setting()
        {
        }
        
        public PersistentMMSTFR002Setting(Sage.ObjectStore.ConnectionData connectionData, System.Type metaDataType) : 
                base(connectionData, metaDataType)
        {
        }
        
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(ColumnName="MMSTFR002SettingID", IsReadOnly=true, AllowOverwrite=Sage.ObjectStore.Builder.AllowOverwriteType.Equal, IsPrimaryKey=true, IsIndexed=true, IsUnique=true, DbType=System.Data.DbType.Int64, Precision=11)]
        public virtual Sage.Common.Data.DbKey MMSTFR002Setting
        {
            get
            {
                return this._MMSTFR002Setting.GetDbKey();
            }
        }
        
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(DbType=System.Data.DbType.Int64, Precision=11)]
        public virtual long AnalysisCode
        {
            get
            {
                return this._AnalysisCode.GetInt64();
            }
            set
            {
                this._AnalysisCode.Value = value;
            }
        }
    }
    
    [System.Runtime.Serialization.DataContractAttribute(IsReference=true, Namespace="http://schemas.sage.com/sage200/2011/")]
    public class PersistentMMSTFR002Settings : Sage.ObjectStore.PersistentObjectCollection
    {
        
        public PersistentMMSTFR002Settings()
        {
        }
        
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        public new virtual PersistentMMSTFR002Setting this[int index]
        {
            get
            {
                return ((PersistentMMSTFR002Setting)(base[index]));
            }
            set
            {
                base[index] = value;
            }
        }
        
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        public override Sage.ObjectStore.PersistentObject Owner
        {
            get
            {
                if ((this.Query.Owner == null))
                {
                    this.Query.Owner = new PersistentMMSTFR002Setting();
                }
                return this.Query.Owner;
            }
        }
        
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        public new virtual PersistentMMSTFR002Setting First
        {
            get
            {
                return ((PersistentMMSTFR002Setting)(base.First));
            }
        }
    }
}
