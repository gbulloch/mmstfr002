namespace Eureka.MMSTFR002.GenerateOrderPlus
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using Sage.Common.Data;
    using Sage.ObjectStore;
    
    
    [System.Runtime.Serialization.DataContractAttribute(IsReference=true, Namespace="http://schemas.sage.com/sage200/2011/")]
    public class MMSTFR002Settings : PersistentMMSTFR002Settings
    {
        
        public MMSTFR002Settings()
        {
        }
        
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        public new virtual MMSTFR002Setting this[int index]
        {
            get
            {
                return ((MMSTFR002Setting)(base[index]));
            }
            set
            {
                base[index] = value;
            }
        }
        
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        public override Sage.ObjectStore.PersistentObject Owner
        {
            get
            {
                if ((this.Query.Owner == null))
                {
                    this.Query.Owner = new MMSTFR002Setting();
                }
                return this.Query.Owner;
            }
        }
        
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        public new virtual MMSTFR002Setting First
        {
            get
            {
                return ((MMSTFR002Setting)(base.First));
            }
        }
    }
}
