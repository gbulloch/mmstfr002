﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sage.Accounting.Stock;
using Sage.Accounting.PurchaseLedger;
using Sage.Common.Data;
using Sage.ObjectStore;
using Sage.ObjectStore.Builder;

namespace Eureka.MMSTFR002.GenerateOrderPlus
{
    /// <summary>
    /// Summary description for EurekaPOPToReorderItem.
    /// </summary>
    [System.Runtime.Serialization.DataContractAttribute(IsReference = true, Namespace = "http://schemas.sage.com/sage200/2011/")]
    public class EurekaPOPToReorderItem : Sage.Accounting.POP.POPToReorderItem
    {
        public const string FIELD_PRODUCTGROUPCODE = "ProductGroupCode";
        protected Sage.ObjectStore.Field _ProductGroupCode;
        public const string FIELD_SUPPLIERREFERENCE = "SupplierReference";
        protected Sage.ObjectStore.Field _SupplierReference;
        public const string FIELD_SUPPLIERPARTREF = "SupplierPartRef";
        protected Sage.ObjectStore.Field _SupplierPartRef;
        public const string FIELD_ANALYSISCODE1 = "AnalysisCode1";
        protected Sage.ObjectStore.Field _AnalysisCode1;
        public const string FIELD_ANALYSISCODE2 = "AnalysisCode2";
        protected Sage.ObjectStore.Field _AnalysisCode2;
        public const string FIELD_ANALYSISCODE3 = "AnalysisCode3";
        protected Sage.ObjectStore.Field _AnalysisCode3;
        public const string FIELD_ANALYSISCODE4 = "AnalysisCode4";
        protected Sage.ObjectStore.Field _AnalysisCode4;
        public const string FIELD_ANALYSISCODE5 = "AnalysisCode5";
        protected Sage.ObjectStore.Field _AnalysisCode5;
        public const string FIELD_ANALYSISCODE6 = "AnalysisCode6";
        protected Sage.ObjectStore.Field _AnalysisCode6;
        public const string FIELD_ANALYSISCODE7 = "AnalysisCode7";
        protected Sage.ObjectStore.Field _AnalysisCode7;
        public const string FIELD_ANALYSISCODE8 = "AnalysisCode8";
        protected Sage.ObjectStore.Field _AnalysisCode8;
        public const string FIELD_ANALYSISCODE9 = "AnalysisCode9";
        protected Sage.ObjectStore.Field _AnalysisCode9;
        public const string FIELD_ANALYSISCODE10 = "AnalysisCode10";
        protected Sage.ObjectStore.Field _AnalysisCode10;
        public const string FIELD_ANALYSISCODE11 = "AnalysisCode11";
        protected Sage.ObjectStore.Field _AnalysisCode11;
        public const string FIELD_ANALYSISCODE12 = "AnalysisCode12";
        protected Sage.ObjectStore.Field _AnalysisCode12;
        public const string FIELD_ANALYSISCODE13 = "AnalysisCode13";
        protected Sage.ObjectStore.Field _AnalysisCode13;
        public const string FIELD_ANALYSISCODE14 = "AnalysisCode14";
        protected Sage.ObjectStore.Field _AnalysisCode14;
        public const string FIELD_ANALYSISCODE15 = "AnalysisCode15";
        protected Sage.ObjectStore.Field _AnalysisCode15;
        public const string FIELD_ANALYSISCODE16 = "AnalysisCode16";
        protected Sage.ObjectStore.Field _AnalysisCode16;
        public const string FIELD_ANALYSISCODE17 = "AnalysisCode17";
        protected Sage.ObjectStore.Field _AnalysisCode17;
        public const string FIELD_ANALYSISCODE18 = "AnalysisCode18";
        protected Sage.ObjectStore.Field _AnalysisCode18;
        public const string FIELD_ANALYSISCODE19 = "AnalysisCode19";
        protected Sage.ObjectStore.Field _AnalysisCode19;
        public const string FIELD_ANALYSISCODE20 = "AnalysisCode20";
        protected Sage.ObjectStore.Field _AnalysisCode20;

        public EurekaPOPToReorderItem()
        {
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "ProductGroup", ColumnName = "Code", FieldPropertyName = "_ProductGroupCode", IsNullable = true, IsBrowsable = true, IsQueryable = true, DbType = System.Data.DbType.AnsiString, Precision = 20)]
        public virtual string ProductGroupCode
        {
            get
            {
                return this._ProductGroupCode.GetString();
            }
            set
            {
                this._ProductGroupCode.Value = value;
            }
        }


        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "SupplierAccountNumber", FieldPropertyName = "_SupplierReference", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 8)]
        public virtual string SupplierReference
        {
            get
            {
                return this._SupplierReference.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode1", FieldPropertyName = "_AnalysisCode1", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode1
        {
            get
            {
                return this._AnalysisCode1.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode2", FieldPropertyName = "_AnalysisCode2", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode2
        {
            get
            {
                return this._AnalysisCode2.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode3", FieldPropertyName = "_AnalysisCode3", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode3
        {
            get
            {
                return this._AnalysisCode3.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode4", FieldPropertyName = "_AnalysisCode4", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode4
        {
            get
            {
                return this._AnalysisCode4.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode5", FieldPropertyName = "_AnalysisCode5", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode5
        {
            get
            {
                return this._AnalysisCode5.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode6", FieldPropertyName = "_AnalysisCode6", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode6
        {
            get
            {
                return this._AnalysisCode6.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode7", FieldPropertyName = "_AnalysisCode7", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode7
        {
            get
            {
                return this._AnalysisCode7.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode8", FieldPropertyName = "_AnalysisCode8", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode8
        {
            get
            {
                return this._AnalysisCode8.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode9", FieldPropertyName = "_AnalysisCode9", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode9
        {
            get
            {
                return this._AnalysisCode9.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode10", FieldPropertyName = "_AnalysisCode10", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode10
        {
            get
            {
                return this._AnalysisCode10.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode11", FieldPropertyName = "_AnalysisCode11", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode11
        {
            get
            {
                return this._AnalysisCode11.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode12", FieldPropertyName = "_AnalysisCode12", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode12
        {
            get
            {
                return this._AnalysisCode12.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode13", FieldPropertyName = "_AnalysisCode13", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode13
        {
            get
            {
                return this._AnalysisCode13.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode14", FieldPropertyName = "_AnalysisCode14", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode14
        {
            get
            {
                return this._AnalysisCode14.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode15", FieldPropertyName = "_AnalysisCode15", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode15
        {
            get
            {
                return this._AnalysisCode15.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode16", FieldPropertyName = "_AnalysisCode16", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode16
        {
            get
            {
                return this._AnalysisCode16.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode17", FieldPropertyName = "_AnalysisCode17", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode17
        {
            get
            {
                return this._AnalysisCode17.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode18", FieldPropertyName = "_AnalysisCode18", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode18
        {
            get
            {
                return this._AnalysisCode18.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode19", FieldPropertyName = "_AnalysisCode19", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode19
        {
            get
            {
                return this._AnalysisCode19.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "PLSupplierAccount", ColumnName = "AnalysisCode20", FieldPropertyName = "_AnalysisCode20", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 60)]
        public virtual string AnalysisCode20
        {
            get
            {
                return this._AnalysisCode20.GetString();
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [Sage.ObjectStore.Builder.MetaDataFieldAttribute(TableName = "StockItemSupplier", ColumnName = "SupplierStockCode", FieldPropertyName = "_SupplierPartRef", IsNullable = true, DbType = System.Data.DbType.AnsiString, Precision = 40)]
        public string SupplierPartRef
        {
            get
            {
                if (string.IsNullOrEmpty(this._SupplierPartRef.GetString()))
                {
                    this._SupplierPartRef.ValueRaw = base.StockItem.PartNumber;
                }

                return _SupplierPartRef.GetString();
            }
        }

        protected override void DeleteInternal()
        {
            Sage.Accounting.POP.POPToReorderItems oPOPToReorderItems = new Sage.Accounting.POP.POPToReorderItems();
            oPOPToReorderItems.Query.Filters.Add(new Filter(Sage.Accounting.POP.POPToReorderItem.FIELD_POPTOREORDERITEM, this.POPToReorderItem));
            oPOPToReorderItems.Find();
            oPOPToReorderItems.First.Delete();
        }

        protected override void UpdateInternal()
        {

            Sage.Accounting.POP.POPToReorderItems oPOPToReorderItems = new Sage.Accounting.POP.POPToReorderItems();
            oPOPToReorderItems.Query.Filters.Add(new Filter(Sage.Accounting.POP.POPToReorderItem.FIELD_POPTOREORDERITEM, this.POPToReorderItem));
            oPOPToReorderItems.Find();
            oPOPToReorderItems.Update();
        }
    }
}
