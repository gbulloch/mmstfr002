﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sage.ObjectStore;
using System.ComponentModel;

namespace Eureka.MMSTFR002.GenerateOrderPlus
{
    /// <summary>
	/// Summary description for EurekaPersistentPOPToReorderItems.
	/// </summary>
    [System.Runtime.Serialization.DataContractAttribute(IsReference = true, Namespace = "http://schemas.sage.com/sage200/2011/")]
    public class EurekaPOPToReorderItems : Sage.Accounting.POP.POPToReorderItems
    {
#if !S200v9 && !S200v92 && !S200v10 && !S200v11 && !S200v12
        public EurekaPOPToReorderItems()
        {
        }

        public EurekaPOPToReorderItems(IContainer container)
        {
            container.Add(this);
        }
#endif

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new EurekaPOPToReorderItem First
        {
            get
            {
                return (EurekaPOPToReorderItem)base.First;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new EurekaPOPToReorderItem this[int iIndex]
        {
            get
            {
                return (EurekaPOPToReorderItem)base[iIndex];
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override PersistentObject Owner
        {
            get
            {
                if (this.Query.Owner == null)
                {
                    this.Query.Owner = new EurekaPOPToReorderItem();
                }
                return this.Query.Owner;
            }
        }

        public override Sage.ObjectStore.Joins Joins
        {
            get
            {
                Sage.ObjectStore.Joins joins1 = base.Joins;
                joins1.Clear();
                //if (joins1.Count == 0)
                //{
                joins1.Add(new Join("POPToReorderItem", "StockItem", "ItemID", "ItemID"));
                joins1.Add(new Join("StockItem", "ProductGroup", "ProductGroupID", "ProductGroupID"));
                Join Join2 = new Join("StockItem", "StockItemSupplier", "ItemID", "ItemID");
                Join2.Type = Sage.Common.Data.JoinType.LeftOuter;
                Join2.Filters.Add(new Filter(Sage.Accounting.Stock.StockItemSupplier.FIELD_PREFERRED, Sage.Common.Data.FilterOperator.Equal, true));
                joins1.Add(Join2);
                joins1.Add(new Join("StockItemSupplier", "PLSupplierAccount", "SupplierID", "PLSupplierAccountID"));

                //}
                return joins1;
            }
        }
    }
}
