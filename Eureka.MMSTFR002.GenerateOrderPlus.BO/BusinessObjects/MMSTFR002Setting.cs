namespace Eureka.MMSTFR002.GenerateOrderPlus
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using Sage.Common.Data;
    using Sage.ObjectStore;
    
    
    public class MMSTFR002Setting : PersistentMMSTFR002Setting
    {
        
        public MMSTFR002Setting()
        {
        }
        
        public MMSTFR002Setting(Sage.ObjectStore.ConnectionData connectionData, System.Type metaDataType) : 
                base(connectionData, metaDataType)
        {
        }
    }
}
