BEGIN TRANSACTION


EXECUTE spCreateTable 'MMSTFR002Setting', 'MMSTFR002SettingID'

EXECUTE spCreateColumn 'MMSTFR002Setting', 'AnalysisCode', 'bigint', '13', '2', 1, 0, 0, 1, '0', 'None'

COMMIT TRANSACTION

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EurekaAddonList]') AND type in (N'U'))
	BEGIN
		IF (SELECT count(*) FROM EurekaAddonList WHERE AddonNumber = 'MMSTFR002')=0
			INSERT INTO [dbo].[EurekaAddonList] (AddonNumber, Description, Module, ReplacesAddonID)
			VALUES ('MMSTFR002', 'Generate Orders Plus.', '4', 116)
		ELSE
			UPDATE [dbo].[EurekaAddonList]
			SET Description = 'Generate Orders Plus.', ReplacesAddonID = 116, Module = '4'
			WHERE AddonNumber = 'MMSTFR002' 
	END
GO

/* This registers the addon in the addon conflicts table
** Sage 200 v6 and above 
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EurekaAddonConflict]') AND type in (N'U'))
	BEGIN
		IF (SELECT count(*) FROM EurekaAddonConflict WHERE AddonNumber = 'MMSTFR002' AND ConflictingAddon = 'MMS116')=0
			INSERT INTO [dbo].[EurekaAddonConflict] (AddonNumber, ConflictingAddon)
			VALUES ('MMSTFR002', 'MMS116')
	END
GO